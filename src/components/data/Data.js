export const nav = [
  {
    text: "home",
    path: "/",
  },
  {
    text: "discover",
    path: "/discover",
  },
]

export const team = [
  {
    list: "16",
    cover: "../images/recs/rec-2.png",
    address: "Redfern, New South Wales 2016",
    name: "Average Weekly Rent Cost: $840",
  },
  {
    list: "15",
    cover: "../images/recs/rec-3.png",
    address: "Ultimo, New South Wales 2007",
    name: "Average Weekly Rent Cost: $900",
  },
  {
    list: "12",
    cover: "../images/recs/rec-1.png",
    address: "Surry Hills, New South Wales 2010",
    name: "Average Weekly Rent Cost: $920",
  }
]

export const team2 = [
  {
    list: "16",
    cover: "../images/recs/rec-2.png",
    address: "Strathfield, New South Wales 2135",
    name: "Average Weekly Rent Cost: $780",
  },
  {
    list: "15",
    cover: "../images/recs/rec-3.png",
    address: "Burwood, New South Wales 2134",
    name: "Average Weekly Rent Cost: $750",
  },
  {
    list: "12",
    cover: "../images/recs/rec-1.png",
    address: "Lidcombe, New South Wales 2141",
    name: "Average Weekly Rent Cost: $740",
  }
]

export const list = [
  {
    id: 1,
    cover: "../images/list/p-1.png",
    name: "LJ Hooker",
    location: "Level 4/185 Broadway st, Ultimo NSW 2007",
    category: "For Rent",
    price: "$750",
    type: "Studio",
  },
  {
    id: 2,
    cover: "../images/list/p-2.png",
    name: "Loyal Property",
    location: "78/267 Bulwara Road, Ultimo NSW 2007",
    category: "For Rent",
    price: "$780",
    type: "Apartment/Unit/Flat",
  },
  {
    id: 3,
    cover: "../images/list/p-7.png",
    name: "Raine&Horne",
    location: "18/292-296 Chalmers Street, Redfern NSW 2016",
    category: "For Rent",
    price: "$780",
    type: "Apartment/Unit/Flat",
  },
  {
    id: 4,
    cover: "../images/list/p-4.png",
    name: "Herringbone Realty",
    location: "45 Douglas Street, Redfern NSW 2016",
    category: "For Rent",
    price: "$850",
    type: "Terrace",
  },
  {
    id: 5,
    cover: "../images/list/p-5.png",
    name: "Taylors Property Management Specialists",
    location: "126 Reservoir Street, Surry Hills NSW 2010",
    category: "For Rent",
    price: "$850",
    type: "Terrace",
  },
  {
    id: 6,
    cover: "../images/list/p-6.png",
    name: "RayWhite",
    location: "14/471 South Dowling Street, Surry Hills NSW 2010",
    category: "For Rent",
    price: "$820",
    type: "Apartment/Unit/Flat",
  },
]
export const prios = [
  {
    icon: <i class='fa-solid fa-trophy'></i>,
    name: "Cheaper Rent Price",
  },
  {
    icon: <i class='fa-solid fa-briefcase'></i>,
    name: "Distance from Central Station",
  },
]
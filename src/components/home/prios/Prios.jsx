import React from "react"
import { useState } from "react"
import Heading from "../../common/Heading"
import { prios } from "../../data/Data"
import Rec1 from "../recs/Rec1"
import Rec2 from "../recs/Rec2"
import Recent from "../../discover/recent/Recent"
import "./prios.css"

const Prios = () => {
	const [showResults1, setShowResults1] = useState(false);
  const [showResults2, setShowResults2] = useState(false);

	const showSearch1 = (e) => {
		e.preventDefault();
		setShowResults1(!showResults1);
	};

  const showSearch2 = (e) => {
		e.preventDefault();
		setShowResults2(!showResults2);
	};

  return (
    <>
      {showResults1 && (
        <Rec2/>
      )}
      {showResults2 && (
        <Rec1/>
      )}
      {showResults1 && (
        <Recent />
      )}
      {showResults2 && (
        <Recent />
      )}
      <section className='awards padding'>
        <div className='container'>
          <Heading title='What is your number one priority?' />

          <div className='content grid4 mtop'>
              <div className='box'>
                <div className='icon'onClick={showSearch1}>
                  <span>{prios[0].icon}</span>
                </div>
                <p>{prios[0].name}</p>
              </div>
							<div className='box'>
                <div className='icon'onClick={showSearch2}>
                  <span>{prios[1].icon}</span>
                </div>
                <p>{prios[1].name}</p>
              </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default Prios
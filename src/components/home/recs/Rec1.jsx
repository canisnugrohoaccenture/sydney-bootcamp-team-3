import React from "react"
import Heading from "../../common/Heading"
import { team } from "../../data/Data"
import "./rec.css"

const Rec1 = () => {
  return (
    <>
      <section className='team background'>
        <div className='container'>
          <Heading title='Our Featured Suburbs'/>

          <div className='content mtop grid3'>
            {team.map((val, index) => (
              <div className='box' key={index}>
                <button className='btn3'>{val.list} Listings</button>
                <div className='details'>
                  <div className='img'>
                    <img src={val.cover} alt='' />
                  </div>
                  <i className='fa fa-location-dot'></i>
                  <label>{val.address}</label>
                  <h4>{val.name}</h4>
                  <h4>{val.transport}</h4>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    </>
  )
}

export default Rec1
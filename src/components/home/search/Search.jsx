import React from "react"
import Heading from "../../common/Heading"
import "./search.css"
import { useState } from "react"
import Prios from "../prios/Prios"

const Hero = () => {
    const [showPrios, setShowPrios] = useState(false);

    const showPrio = (e) => {
      e.preventDefault();
      setShowPrios(!showPrios);
    };
  
  return (
    <>
      {showPrios && (
        <Prios/>
      )}
      <section className='hero'>
        <div className='container'>
          <Heading title='Search Your Next Home ' subtitle='Find alternative rental properties' />
            <form className='flex' id='searchBox'>
              <div className='box' id='chatBox'>
                <input type='text' placeholder="Describe your next home to Rental Rescue Bot..." id='chatBotInput'/>
              </div>
              <button id="searchButton" onClick={showPrio}>
                Search 
              </button>
            </form>
        </div>
      </section>
    </>
  )
}

export default Hero

import React from "react"
import Header from "../common/header/Header"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Home from "../home/Home"
import Footer from "../common/footer/Footer"
import Discover from "../discover/Discover"
import Login from "../login/Login"

const Pages = () => {
  return (
    <>
      <Router>
        <Header />
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/discover' component={Discover} />
          <Route exact path='/login' component={Login} />
        </Switch>
        <Footer />
      </Router>
    </>
  )
}

export default Pages

import React from "react"
import Back from "../common/Back"
import "./recent/recent.css"
import img from "../images/about.jpg"
import Location from "./location/Location"

const Discover = () => {
  return (
    <>
      <section className='blog-out mb'>
        <Back name='Discover' title='Discover' cover={img} />
        <div className='container recent'>
          <Location/>
        </div>
      </section>
    </>
  )
}

export default Discover
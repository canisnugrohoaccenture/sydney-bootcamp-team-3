import React from "react"
import Heading from "../../common/Heading"
import "./style.css"

const Location = () => {
  return (
    <>
      <section className='location padding'>
        <div className='container'id='heatmapContainer'>
          <Heading title='Rent Heat Map'/>
          <iframe src="https://heatmaps.com.au/" title="heatmap" frameborder="0" id="heatmap" scrolling="no"></iframe>
        </div>
      </section>
    </>
  )
}

export default Location